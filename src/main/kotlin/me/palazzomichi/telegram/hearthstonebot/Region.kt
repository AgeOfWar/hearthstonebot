package me.palazzomichi.telegram.hearthstonebot

enum class Region(val symbol: String) {
    UNITED_STATES("us"), EUROPEAN_UNION("eu"), SOUTH_KOREA("kr"), CHINA("tw");
}