package me.palazzomichi.telegram.hearthstonebot

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import org.dmfs.httpessentials.httpurlconnection.HttpUrlConnectionExecutor
import org.dmfs.oauth2.client.BasicOAuth2AuthorizationProvider
import org.dmfs.oauth2.client.BasicOAuth2Client
import org.dmfs.oauth2.client.BasicOAuth2ClientCredentials
import org.dmfs.oauth2.client.OAuth2Grant
import org.dmfs.oauth2.client.grants.ClientCredentialsGrant
import org.dmfs.oauth2.client.scope.BasicScope
import org.dmfs.rfc3986.encoding.Precoded
import org.dmfs.rfc3986.uris.LazyUri
import org.dmfs.rfc5545.Duration
import java.net.URI
import java.util.*

class HearthstoneApi(
    clientId: String,
    clientSecret: String,
    region: Region
) {
    private val gson: Gson = with(GsonBuilder()) {
        setPrettyPrinting()
        create()
    }
    private val apiUrl = "https://${region.symbol}.api.blizzard.com/hearthstone"
    private val grant: OAuth2Grant
    private val executor = HttpUrlConnectionExecutor()
    init {
        val redirectUri = LazyUri(Precoded("http://localhost"))
        val provider = BasicOAuth2AuthorizationProvider(
            URI.create("https://${region.symbol}.battle.net/oauth/authorize"),
            URI.create("https://${region.symbol}.battle.net/oauth/token"),
            Duration(1, 1, 0)
        )
        val credentials = BasicOAuth2ClientCredentials(clientId, clientSecret)
        val client = BasicOAuth2Client(provider, credentials, redirectUri)
        grant = ClientCredentialsGrant(client, BasicScope())
    }

    fun cards(region: Region,
              locale: Locale,
              pageSize: Int,
              page: Int = 1,
              textFilter: String? = null,
              set: String? = null,
              `class`: String? = null,
              manaCost: Int? = null,
              attack: Int? = null,
              health: Int? = null,
              collectible: Boolean? = null,
              rarity: String? = null,
              type: String? = null,
              minionType: String? = null,
              keyword: String? = null,
              gameMode: String? = null,
              sort: String? = null,
              order: String? = null
    ): Cards {
        val parameters = mapOf(
            "region" to region.symbol,
            "locale" to (locale.adjust() ?: Locale.US),
            "pageSize" to pageSize,
            "textFilter" to textFilter,
            "page" to page,
            "set" to set,
            "class" to `class`,
            "manaCost" to manaCost,
            "attack" to attack,
            "health" to health,
            "collectible" to collectible?.let { if (it) 1 else 0 },
            "rarity" to rarity,
            "type" to type,
            "minionType" to minionType,
            "keyword" to keyword,
            "gameMode" to gameMode,
            "sort" to sort,
            "order" to order
        )
        return execute("cards", parameters)
    }

    private inline fun <reified T> execute(path: String, parameters: Map<String, Any?>): T {
        val token = getToken()
        val reader = post("$apiUrl/$path", parameters.filterValues { it != null } + ("access_token" to token)).reader()
        return gson.fromJson(reader, T::class.java)
    }

    private fun getToken(): String = grant.accessToken(executor).accessToken().toString()

    private fun Locale.adjust(): Locale? {
        return when (this) {
            Locale.ITALIAN, Locale.ITALY -> Locale.ITALY
            Locale.ENGLISH, Locale.US -> Locale.US
            Locale.UK -> Locale.UK
            else -> null
        }
    }
}
