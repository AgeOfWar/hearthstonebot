package me.palazzomichi.telegram.hearthstonebot

import java.io.IOException
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL
import java.net.URLEncoder


private const val CONNECTION_TIMEOUT = 0
private const val READ_TIMEOUT = 0

@Throws(IOException::class)
fun post(url: String, parameters: Map<String, Any?>): InputStream {
    val params = buildString {
        parameters.forEach { (key, value) ->
            append(key)
            append("=")
            append(URLEncoder.encode(value.toString(), "UTF-8"))
            append("&")
        }
    }
    with(URL("$url?$params").openConnection() as HttpURLConnection) {
        requestMethod = "GET"
        useCaches = false
        connectTimeout = CONNECTION_TIMEOUT
        readTimeout = READ_TIMEOUT
        setRequestProperty("Accept", "application/json")
        return inputStream
    }
}
