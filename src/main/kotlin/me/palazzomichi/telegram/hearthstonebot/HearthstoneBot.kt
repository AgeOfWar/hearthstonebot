package me.palazzomichi.telegram.hearthstonebot

import io.github.ageofwar.telejam.Bot
import io.github.ageofwar.telejam.LongPollingBot
import io.github.ageofwar.telejam.commands.Command
import io.github.ageofwar.telejam.commands.CommandHandler
import io.github.ageofwar.telejam.inline.InlineQuery
import io.github.ageofwar.telejam.inline.InlineQueryHandler
import io.github.ageofwar.telejam.inline.InlineQueryResultPhoto
import io.github.ageofwar.telejam.messages.TextMessage
import io.github.ageofwar.telejam.methods.AnswerInlineQuery
import io.github.ageofwar.telejam.methods.SendMessage
import io.github.ageofwar.telejam.text.Text
import java.util.*

class HearthstoneBot(
    bot: Bot,
    private val hearthstoneApi: HearthstoneApi,
    private val helpMessage: Text
) : LongPollingBot(bot) {
    init {
        events.apply {
            registerCommand(HelpCommand(bot, helpMessage), "start")
            registerUpdateHandler(CardInlineQuery(bot, hearthstoneApi))
        }
    }
}

class HelpCommand(
    private val bot: Bot,
    private val helpMessage: Text
) : CommandHandler {
    override fun onCommand(command: Command, message: TextMessage) {
        val sendMessage = SendMessage()
            .replyToMessage(message)
            .text(helpMessage)
        bot.execute(sendMessage)
    }
}

class CardInlineQuery(
    private val bot: Bot,
    private val hearthstoneApi: HearthstoneApi
) : InlineQueryHandler {
    override fun onInlineQuery(inlineQuery: InlineQuery) {
        val sender = inlineQuery.sender
        val query = inlineQuery.query
        var page = 1
        var locale: Locale = sender.locale
        var textFilter: String? = null
        var set: String? = null
        var `class`: String? = null
        var manaCost: Int? = null
        var attack: Int? = null
        var health: Int? = null
        var collectible: Boolean? = null
        var rarity: String? = null
        var type: String? = null
        var minionType: String? = null
        var keyword: String? = null
        var gameMode: String? = null
        var sort: String? = null
        var order: String? = null
        query.split(Regex("\\s")).forEach { arg ->
            val filter = arg.split(':', limit = 2)
            if (filter.size == 2) {
                val (key, value) = filter
                when (key) {
                    "page" -> value.toIntOrNull()?.let { page = it }
                    "set" -> set = value
                    "class" -> `class` = value
                    "mana" -> value.toIntOrNull()?.let { manaCost = it }
                    "attack" -> value.toIntOrNull()?.let { attack = it }
                    "health" -> value.toIntOrNull()?.let { health = it }
                    "collectible" -> value.toIntOrNull()?.let { collectible = it == 1 }
                    "rarity" -> rarity = value
                    "type" -> type = value
                    "minionType" -> minionType = value
                    "keyword" -> keyword = value
                    "gamemode" -> gameMode = value
                    "sort" -> sort = value
                    "order" -> order = value
                    "locale" -> locale = Locale.forLanguageTag(value)
                }
            } else if (filter.size == 1) {
                if (textFilter == null) {
                    textFilter = filter.single()
                } else {
                    textFilter += " " + filter.single()
                }
            }
        }
        val cards = hearthstoneApi.cards(
            Region.EUROPEAN_UNION,
            locale,
            50,
            page,
            textFilter,
            set,
            `class`,
            manaCost,
            attack,
            health,
            collectible,
            rarity,
            type,
            minionType,
            keyword,
            gameMode,
            sort,
            order
        )
        val answerInlineQuery = AnswerInlineQuery()
            .inlineQuery(inlineQuery)
            .isPersonal(true)
            .cacheTime(300)
            .results(*cards.cards.mapNotNullTo(mutableSetOf()) {
                if (it.slug.isEmpty() || it.name.isEmpty() || it.imageUrl.isEmpty()) {
                    null
                } else InlineQueryResultPhoto(
                    it.id.toString(),
                    it.imageUrl,
                    it.imageUrl,
                    375,
                    518,
                    it.name,
                    null,
                    Text.italic(it.flavorText),
                    null,
                    null
                )
            }.toTypedArray())
        bot.execute(answerInlineQuery)
    }
}