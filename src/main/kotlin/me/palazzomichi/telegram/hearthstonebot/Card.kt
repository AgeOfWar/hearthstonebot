package me.palazzomichi.telegram.hearthstonebot

import com.google.gson.*
import com.google.gson.annotations.JsonAdapter
import com.google.gson.annotations.SerializedName
import java.lang.reflect.Type

data class Card(
    val id: Long,
    @JsonAdapter(CollectibleSerializer::class) val collectible: Boolean,
    val slug: String,
    val classId: Byte,
    val cardTypeId: Byte,
    val cardSetId: Byte,
    val rarityId: Byte,
    val artistName: String,
    val manaCost: Int,
    val name: String,
    val text: String,
    @SerializedName("image") val imageUrl: String,
    @SerializedName("imageGold") val imageGoldUrl: String,
    val flavorText: String
)

private class CollectibleSerializer : JsonSerializer<Boolean>, JsonDeserializer<Boolean> {
    override fun serialize(collectible: Boolean, typeOfSrc: Type, context: JsonSerializationContext): JsonElement {
        return JsonPrimitive(if (collectible) 1 else 0)
    }

    override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): Boolean {
        return json.asInt == 1
    }
}
