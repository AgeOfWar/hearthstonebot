package me.palazzomichi.telegram.hearthstonebot

import io.github.ageofwar.telejam.Bot
import io.github.ageofwar.telejam.text.Text
import java.io.File
import java.io.FileReader
import java.io.FileWriter
import java.util.*


const val BOT_PROPERTIES_PATH = "bot.properties"

fun main() {
    if (File(BOT_PROPERTIES_PATH).exists()) {
        val properties = BotProperties.load(BOT_PROPERTIES_PATH)
        val (clientId, clientSecret, token, helpMessage) = properties
        val hearthstoneApi = HearthstoneApi(clientId, clientSecret, Region.UNITED_STATES)
        HearthstoneBot(Bot.fromToken(token), hearthstoneApi, helpMessage).run()
    } else {
        BotProperties("", "", "", Text.EMPTY).save(BOT_PROPERTIES_PATH)
    }
}

data class BotProperties(
    val clientId: String,
    val clientSecret: String,
    val token: String,
    val helpMessage: Text
) {
    fun save(path: String) {
        val properties = Properties().apply {
            setProperty("client-id", clientId)
            setProperty("client-secret", clientSecret)
            setProperty("bot-token", token)
            setProperty("help-message", helpMessage.toHtmlString())
        }
        properties.store(FileWriter(path), null)
    }

    companion object {
        fun load(path: String): BotProperties {
            val properties = Properties().apply { load(FileReader(path)) }
            val clientId = properties.getProperty("client-id")!!
            val clientSecret = properties.getProperty("client-secret")!!
            val token = properties.getProperty("bot-token")!!
            val helpMessage = Text.parseHtml(properties.getProperty("help-message")!!)
            return BotProperties(clientId, clientSecret, token, helpMessage)
        }
    }
}
