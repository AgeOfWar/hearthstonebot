package me.palazzomichi.telegram.hearthstonebot

data class Cards(
    val cards: List<Card>,
    val cardCount: Int,
    val page: Int,
    val pageCount: Int
)